<?php

namespace BitAndBlack\QrCode;

use Color\Value\ValueInterface;

interface QrCodeInterface
{
    public function getText(): string;

    public function getSize(): int;

    public function getMargin(): int;

    /**
     * Returns the foreground color
     *
     * @return ValueInterface
     */
    public function getForegroundColor(): ValueInterface;

    /**
     * Returns the background color
     *
     * @return ValueInterface
     */
    public function getBackgroundColor(): ValueInterface;

    public function getEncoding(): string;

    public function getRoundBlockSize(): bool;

    public function getErrorCorrectionLevel(): ErrorCorrectionLevel;

    public function getLogoPath(): ?string;

    public function getLogoWidth(): ?int;

    public function getLogoHeight(): ?int;

    public function getLabel(): ?string;

    public function getLabelFontPath(): ?string;

    public function getLabelFontSize(): int;

    public function getLabelAlignment(): string;

    public function getLabelMargin(): array;

    public function getValidateResult(): bool;

    public function getWriterOptions(): array;

    public function getContentType(): string;

    public function setWriterRegistry(WriterRegistryInterface $writerRegistry): self;

    public function writeString(): string;

    public function writeDataUri(): string;

    public function writeFile(string $path): self;

    public function getData(): array;
}
