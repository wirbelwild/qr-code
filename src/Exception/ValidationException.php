<?php

namespace BitAndBlack\QrCode\Exception;

class ValidationException extends QrCodeException
{
}
