<?php

namespace BitAndBlack\QrCode\Exception;

use Exception;

abstract class QrCodeException extends Exception
{
}
