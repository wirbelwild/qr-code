<?php

namespace BitAndBlack\QrCode\Exception;

class MissingFunctionException extends QrCodeException
{
}
