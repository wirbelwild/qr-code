<?php

namespace BitAndBlack\QrCode\Exception;

class InvalidPathException extends QrCodeException
{
}
