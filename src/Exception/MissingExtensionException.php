<?php

namespace BitAndBlack\QrCode\Exception;

class MissingExtensionException extends QrCodeException
{
}
