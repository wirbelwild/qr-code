<?php

namespace BitAndBlack\QrCode\Exception;

class UnsupportedExtensionException extends QrCodeException
{
}
