<?php

namespace BitAndBlack\QrCode\Exception;

class MissingLogoHeightException extends QrCodeException
{
}
