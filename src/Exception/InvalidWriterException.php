<?php

namespace BitAndBlack\QrCode\Exception;

class InvalidWriterException extends QrCodeException
{
}
