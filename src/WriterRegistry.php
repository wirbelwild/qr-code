<?php

namespace BitAndBlack\QrCode;

use BitAndBlack\QrCode\Exception\InvalidWriterException;
use BitAndBlack\QrCode\Writer\BinaryWriter;
use BitAndBlack\QrCode\Writer\DebugWriter;
use BitAndBlack\QrCode\Writer\EpsWriter;
use BitAndBlack\QrCode\Writer\PngWriter;
use BitAndBlack\QrCode\Writer\SvgWriter;
use BitAndBlack\QrCode\Writer\WriterInterface;

class WriterRegistry implements WriterRegistryInterface
{
    private array $writers = [];
    private ?WriterInterface $defaultWriter = null;

    public function loadDefaultWriters(): void
    {
        if (count($this->writers) > 0) {
            return;
        }

        $this->addWriters([
            new BinaryWriter(),
            new DebugWriter(),
            new EpsWriter(),
            new PngWriter(),
            new SvgWriter(),
        ]);

        $this->setDefaultWriter('png');
    }

    public function addWriters(iterable $writers): void
    {
        foreach ($writers as $writer) {
            $this->addWriter($writer);
        }
    }

    public function addWriter(WriterInterface $writer): void
    {
        $this->writers[$writer->getName()] = $writer;
    }

    /**
     * @throws InvalidWriterException
     */
    public function getWriter(string $name): WriterInterface
    {
        $this->assertValidWriter($name);

        return $this->writers[$name];
    }

    /**
     * @throws InvalidWriterException
     */
    public function getDefaultWriter(): ?WriterInterface
    {
        if ($this->defaultWriter instanceof WriterInterface) {
            return $this->defaultWriter;
        }

        throw new InvalidWriterException('Please set the default writer via the second argument of addWriter');
    }

    public function setDefaultWriter(string $name): void
    {
        $this->defaultWriter = $this->writers[$name];
    }

    public function getWriters(): array
    {
        return $this->writers;
    }

    /**
     * @throws InvalidWriterException
     */
    private function assertValidWriter(string $name): void
    {
        if (!isset($this->writers[$name])) {
            throw new InvalidWriterException('Invalid writer "' . $name . '"');
        }
    }
}
