<?php

namespace BitAndBlack\QrCode\Writer;

use BitAndBlack\QrCode\Exception\GenerateImageException;
use BitAndBlack\QrCode\Exception\InvalidLogoException;
use BitAndBlack\QrCode\Exception\MissingExtensionException;
use BitAndBlack\QrCode\Exception\MissingLogoHeightException;
use BitAndBlack\QrCode\Exception\ValidationException;
use BitAndBlack\QrCode\QrCodeInterface;
use SimpleXMLElement;

/**
 * @see \BitAndBlack\QrCode\Tests\Writer\SvgWriterTest
 */
class SvgWriter extends AbstractWriter
{
    /**
     * @param QrCodeInterface $qrCode
     * @return string
     * @throws GenerateImageException
     * @throws InvalidLogoException
     * @throws MissingExtensionException
     * @throws MissingLogoHeightException
     * @throws ValidationException
     */
    public function writeString(QrCodeInterface $qrCode): string
    {
        if ($qrCode->getValidateResult()) {
            throw new ValidationException('Built-in validation reader can not check SVG images: please disable via setValidateResult(false)');
        }

        $data = $qrCode->getData();

        $svg = new SimpleXMLElement('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" />');
        $svg->addAttribute('version', '1.1');
        $svg->addAttribute('width', $data['outer_width'] . 'px');
        $svg->addAttribute('height', $data['outer_height'] . 'px');
        $svg->addAttribute('viewBox', '0 0 ' . $data['outer_width'] . ' ' . $data['outer_height']);
        $svg->addChild('defs');

        // Block definition
        $blockDefinition = $svg->defs->addChild('rect');
        $blockDefinition->addAttribute('id', 'block');
        $blockDefinition->addAttribute('width', strval($data['block_size']));
        $blockDefinition->addAttribute('height', strval($data['block_size']));
        $blockDefinition->addAttribute('fill', (string) $qrCode->getForegroundColor()->getHEX());

        // Background
        $background = $svg->addChild('rect');
        $background->addAttribute('x', '0');
        $background->addAttribute('y', '0');
        $background->addAttribute('width', strval($data['outer_width']));
        $background->addAttribute('height', strval($data['outer_height']));
        $background->addAttribute('fill', (string) $qrCode->getBackgroundColor()->getHEX());

        foreach ($data['matrix'] as $row => $values) {
            foreach ($values as $column => $value) {
                if (1 === $value) {
                    $block = $svg->addChild('use');
                    $block->addAttribute('x', strval($data['margin_left'] + $data['block_size'] * $column));
                    $block->addAttribute('y', strval($data['margin_left'] + $data['block_size'] * $row));
                    $block->addAttribute('xlink:href', '#block', 'http://www.w3.org/1999/xlink');
                }
            }
        }

        if (null !== $qrCode->getLogoPath()) {
            $this->addLogo($svg, $data['outer_width'], $data['outer_height'], $qrCode->getLogoPath(), $qrCode->getLogoWidth(), $qrCode->getLogoHeight());
        }

        $xml = $svg->asXML();

        if (!is_string($xml)) {
            throw new GenerateImageException('Unable to save SVG XML');
        }

        $options = $qrCode->getWriterOptions();
        if (isset($options['exclude_xml_declaration']) && $options['exclude_xml_declaration']) {
            $xml = str_replace("<?xml version=\"1.0\"?>\n", '', $xml);
        }

        return $xml;
    }

    /**
     * @param SimpleXMLElement $svg
     * @param int $imageWidth
     * @param int $imageHeight
     * @param string $logoPath
     * @param int|null $logoWidth
     * @param int|null $logoHeight
     * @throws GenerateImageException
     * @throws InvalidLogoException
     * @throws MissingExtensionException
     * @throws MissingLogoHeightException
     */
    private function addLogo(SimpleXMLElement $svg, int $imageWidth, int $imageHeight, string $logoPath, int|null $logoWidth = null, int|null $logoHeight = null): void
    {
        $mimeType = $this->getMimeType($logoPath);
        $imageData = file_get_contents($logoPath);

        if (!is_string($imageData)) {
            throw new InvalidLogoException('Invalid logo at path "' . $logoPath . '"');
        }

        if (null === $logoHeight) {
            if ('image/svg+xml' === $mimeType) {
                throw new MissingLogoHeightException('SVG Logos require an explicit height set via setLogoSize($width, $height)');
            } else {
                $logoImage = imagecreatefromstring($imageData);

                if (false === $logoImage) {
                    throw new GenerateImageException('Unable to generate image: check your GD installation');
                }

                $aspectRatio = $logoWidth / imagesx($logoImage);
                $logoHeight = (int) imagesy($logoImage) * $aspectRatio;
            }
        }

        $imageDefinition = $svg->addChild('image');
        $imageDefinition->addAttribute('x', strval($imageWidth / 2 - $logoWidth / 2));
        $imageDefinition->addAttribute('y', strval($imageHeight / 2 - $logoHeight / 2));
        $imageDefinition->addAttribute('width', strval($logoWidth));
        $imageDefinition->addAttribute('height', strval($logoHeight));
        $imageDefinition->addAttribute('preserveAspectRatio', 'none');
        $imageDefinition->addAttribute('xlink:href', 'data:' . $mimeType . ';base64,' . base64_encode($imageData));
    }

    /**
     * @param string $path
     * @return string
     * @throws InvalidLogoException
     * @throws MissingExtensionException
     */
    private function getMimeType(string $path): string
    {
        if (!function_exists('mime_content_type')) {
            throw new MissingExtensionException('You need the ext-fileinfo extension to determine logo mime type');
        }

        $mimeType = mime_content_type($path);

        if (!is_string($mimeType)) {
            throw new InvalidLogoException('Could not determine mime type');
        }

        // Passing mime type image/svg results in invisible images
        if ('image/svg' === $mimeType) {
            return 'image/svg+xml';
        }

        return $mimeType;
    }

    public static function getContentType(): string
    {
        return 'image/svg+xml';
    }

    public static function getSupportedExtensions(): array
    {
        return ['svg'];
    }

    public function getName(): string
    {
        return 'svg';
    }
}
