<?php

namespace BitAndBlack\QrCode\Writer;

use BackedEnum;
use BitAndBlack\QrCode\QrCodeInterface;
use Exception;
use ReflectionClass;
use ReflectionException;

class DebugWriter extends AbstractWriter
{
    /**
     * @param QrCodeInterface $qrCode
     * @return string
     * @throws ReflectionException
     */
    public function writeString(QrCodeInterface $qrCode): string
    {
        $data = [];
        $skip = ['getData'];

        $reflectionClass = new ReflectionClass($qrCode);
        foreach ($reflectionClass->getMethods() as $method) {
            $methodName = $method->getShortName();
            if (str_starts_with($methodName, 'get') && 0 === $method->getNumberOfParameters() && !in_array($methodName, $skip)) {
                $value = $qrCode->{$methodName}();
                if (is_array($value) && !is_object(current($value))) {
                    $value = '[' . implode(', ', $value) . ']';
                } elseif (is_bool($value)) {
                    $value = $value ? 'true' : 'false';
                } elseif (is_string($value)) {
                    $value = '"' . $value . '"';
                } elseif (null === $value) {
                    $value = 'null';
                } elseif ($value instanceof BackedEnum) {
                    $value = $value->value;
                }

                try {
                    $data[] = $methodName . ': ' . $value;
                } catch (Exception) {
                }
            }
        }

        $string = implode(" \n", $data);

        return $string;
    }

    public static function getContentType(): string
    {
        return 'text/plain';
    }

    public function getName(): string
    {
        return 'debug';
    }
}
