<?php

namespace BitAndBlack\QrCode\Writer;

use BitAndBlack\QrCode\QrCodeInterface;
use Color\Value\CMY;
use Color\Value\CMYK;

/**
 * Class EpsWriter
 *
 * @package BitAndBlack\QrCode\Writer
 * @see \BitAndBlack\QrCode\Tests\Writer\EpsWriterTest
 */
class EpsWriter extends AbstractWriter
{
    /**
     * @param QrCodeInterface $qrCode
     * @return string
     */
    public function writeString(QrCodeInterface $qrCode): string
    {
        $data = $qrCode->getData();

        $backgroundColorValues = $qrCode->getBackgroundColor()->getRGB()->getValues();
        $backgroundColorValues = $this->changeValueToInt($backgroundColorValues, 255);
        $backgroundColor = implode(' ', $backgroundColorValues) . ' setrgbcolor';

        $isBackgroundCMYOrCMYK = $qrCode->getBackgroundColor() instanceof CMYK
            || $qrCode->getBackgroundColor() instanceof CMY;
        
        $isForegroundCMYOrCMYK = $qrCode->getForegroundColor() instanceof CMYK
            || $qrCode->getForegroundColor() instanceof CMY;
        
        if ($isBackgroundCMYOrCMYK) {
            $backgroundColorValues = $qrCode->getBackgroundColor()->getCMYK()->getValues();
            $backgroundColorValues = $this->changeValueToInt($backgroundColorValues, 100);
            $backgroundColor = implode(' ', $backgroundColorValues) . ' setcmykcolor';
        }

        $foregroundColorValues = $qrCode->getForegroundColor()->getRGB()->getValues();
        $foregroundColorValues = $this->changeValueToInt($foregroundColorValues, 255);
        $foregroundColor = implode(' ', $foregroundColorValues) . ' setrgbcolor';

        if ($isForegroundCMYOrCMYK) {
            $foregroundColorValues = $qrCode->getForegroundColor()->getCMYK()->getValues();
            $foregroundColorValues = $this->changeValueToInt($foregroundColorValues, 100);
            $foregroundColor = implode(' ', $foregroundColorValues) . ' setcmykcolor';
        }
        
        $epsData = [];
        $epsData[] = '%!PS-Adobe-3.0 EPSF-3.0';
        $epsData[] = '%%BoundingBox: 0 0 ' . $data['outer_width'] . ' ' . $data['outer_height'];
        $epsData[] = '/F { rectfill } def';
        $epsData[] = $backgroundColor;
        $epsData[] = '0 0 ' . $data['outer_width'] . ' ' . $data['outer_height'] . ' F';
        $epsData[] = $foregroundColor;

        foreach ($data['matrix'] as $row => $values) {
            foreach ($values as $column => $value) {
                if (1 === $value) {
                    $x = $data['margin_left'] + $data['block_size'] * $column;
                    $y = $data['margin_left'] + $data['block_size'] * $row;
                    $epsData[] = $x . ' ' . $y . ' ' . $data['block_size'] . ' ' . $data['block_size'] . ' F';
                }
            }
        }

        return implode(PHP_EOL, $epsData);
    }

    public static function getContentType(): string
    {
        return 'image/eps';
    }

    public static function getSupportedExtensions(): array
    {
        return ['eps'];
    }

    public function getName(): string
    {
        return 'eps';
    }

    /**
     * @param array $colorValues
     * @param int $percent
     * @return array
     */
    private function changeValueToInt(array $colorValues, int $percent)
    {
        return array_map(
            static fn ($colorValue) => $colorValue / $percent,
            $colorValues
        );
    }
}
