<?php

namespace BitAndBlack\QrCode\Writer;

use BitAndBlack\QrCode\QrCodeInterface;

class BinaryWriter extends AbstractWriter
{
    public function writeString(QrCodeInterface $qrCode): string
    {
        $rows = [];
        $data = $qrCode->getData();
        foreach ($data['matrix'] as $row) {
            $values = '';
            foreach ($row as $value) {
                $values .= $value;
            }
            $rows[] = $values;
        }

        return implode(PHP_EOL, $rows);
    }

    public static function getContentType(): string
    {
        return 'text/plain';
    }

    public static function getSupportedExtensions(): array
    {
        return ['bin', 'txt'];
    }

    public function getName(): string
    {
        return 'binary';
    }
}
