<?php

namespace BitAndBlack\QrCode\Writer;

use BitAndBlack\QrCode\Exception\GenerateImageException;
use BitAndBlack\QrCode\Exception\MissingFunctionException;
use BitAndBlack\QrCode\Exception\ValidationException;
use BitAndBlack\QrCode\LabelAlignment;
use BitAndBlack\QrCode\QrCodeInterface;
use Color\Value\ValueInterface;
use Zxing\QrReader;

/**
 * Class PngWriter
 * Outputs the QR as PNG.
 * Note that colors with alpha channels like RGBA will not affect the transparency of the image.
 * The alpha information is getting used for creating the color. For example rgba(255, 255, 255, .5) will return grey.
 *
 * @package BitAndBlack\QrCode\Writer
 */
class PngWriter extends AbstractWriter
{
    /**
     * @param QrCodeInterface $qrCode
     * @return string
     * @throws GenerateImageException
     * @throws MissingFunctionException
     * @throws ValidationException
     */
    public function writeString(QrCodeInterface $qrCode): string
    {
        $image = $this->createImage($qrCode->getData(), $qrCode);
        
        if (false === $image) {
            return '';
        }

        if (null !== $qrCode->getLogoPath()) {
            $image = $this->addLogo($image, $qrCode->getLogoPath(), $qrCode->getLogoWidth(), $qrCode->getLogoHeight());
        }

        if (null !== $qrCode->getLabel()) {
            $image = $this->addLabel($image, $qrCode->getLabel(), $qrCode->getLabelFontPath(), $qrCode->getLabelFontSize(), $qrCode->getLabelAlignment(), $qrCode->getLabelMargin(), $qrCode->getForegroundColor(), $qrCode->getBackgroundColor());
        }

        $string = $this->imageToString($image);

        if ($qrCode->getValidateResult()) {
            $reader = new QrReader($string, QrReader::SOURCE_TYPE_BLOB);
            if ($reader->text() !== $qrCode->getText()) {
                throw new ValidationException(
                    'Built-in validation reader read "' . $reader->text() . '" instead of "' . $qrCode->getText() . '".
                     Adjust your parameters to increase readability or disable built-in validation.'
                );
            }
        }

        return $string;
    }

    /**
     * @param array $data
     * @param QrCodeInterface $qrCode
     * @return false|resource
     * @throws GenerateImageException
     */
    private function createImage(array $data, QrCodeInterface $qrCode)
    {
        $baseSize = $qrCode->getRoundBlockSize() ? $data['block_size'] : 25;

        $baseImage = $this->createBaseImage($baseSize, $data, $qrCode);
        $interpolatedImage = $this->createInterpolatedImage($baseImage, $data, $qrCode);

        return $interpolatedImage;
    }

    /**
     * @param int $baseSize
     * @param array $data
     * @param QrCodeInterface $qrCode
     * @return resource
     * @throws GenerateImageException
     */
    private function createBaseImage(int $baseSize, array $data, QrCodeInterface $qrCode)
    {
        $image = imagecreatetruecolor($data['block_count'] * $baseSize, $data['block_count'] * $baseSize);

        if (false === $image) {
            throw new GenerateImageException('Unable to generate image: check your GD installation');
        }

        $foregroundColor = imagecolorallocatealpha(
            $image,
            $qrCode->getForegroundColor()->getRGB()->getValue('R'),
            $qrCode->getForegroundColor()->getRGB()->getValue('G'),
            $qrCode->getForegroundColor()->getRGB()->getValue('B'),
            0
        );
        
        $backgroundColor = imagecolorallocatealpha(
            $image,
            $qrCode->getBackgroundColor()->getRGB()->getValue('R'),
            $qrCode->getBackgroundColor()->getRGB()->getValue('G'),
            $qrCode->getBackgroundColor()->getRGB()->getValue('B'),
            0
        );

        imagefill($image, 0, 0, $backgroundColor);

        foreach ($data['matrix'] as $row => $values) {
            foreach ($values as $column => $value) {
                if (1 === $value) {
                    imagefilledrectangle($image, $column * $baseSize, $row * $baseSize, (int) ($column + 1) * $baseSize, (int) ($row + 1) * $baseSize, $foregroundColor);
                }
            }
        }

        return $image;
    }

    /**
     * @param resource $baseImage
     * @param array $data
     * @param QrCodeInterface $qrCode
     * @return false|resource
     * @throws GenerateImageException
     */
    private function createInterpolatedImage($baseImage, array $data, QrCodeInterface $qrCode)
    {
        $image = imagecreatetruecolor($data['outer_width'], $data['outer_height']);

        if (false === $image) {
            throw new GenerateImageException('Unable to generate image: check your GD installation');
        }

        $backgroundColor = imagecolorallocatealpha(
            $image,
            $qrCode->getBackgroundColor()->getRGB()->getValue('R'),
            $qrCode->getBackgroundColor()->getRGB()->getValue('G'),
            $qrCode->getBackgroundColor()->getRGB()->getValue('B'),
            0
        );
        
        imagefill($image, 0, 0, $backgroundColor);
        imagecopyresampled($image, $baseImage, (int) $data['margin_left'], (int) $data['margin_left'], 0, 0, (int) $data['inner_width'], (int) $data['inner_height'], imagesx($baseImage), imagesy($baseImage));

        return $image;
    }

    /**
     * @param resource $sourceImage
     * @param string $logoPath
     * @param int|null $logoWidth
     * @param int|null $logoHeight
     * @return mixed
     * @throws GenerateImageException
     */
    private function addLogo($sourceImage, string $logoPath, int|null $logoWidth = null, int|null $logoHeight = null)
    {
        $logoImage = imagecreatefromstring((string) file_get_contents($logoPath));

        if (false === $logoImage) {
            throw new GenerateImageException('Unable to generate image: check your GD installation');
        }

        $logoSourceWidth = imagesx($logoImage);
        $logoSourceHeight = imagesy($logoImage);

        if (null === $logoWidth) {
            $logoWidth = $logoSourceWidth;
        }

        if (null === $logoHeight) {
            $aspectRatio = $logoWidth / $logoSourceWidth;
            $logoHeight = $logoSourceHeight * $aspectRatio;
        }
        
        $logoWidth = (int) $logoWidth;
        $logoHeight = (int) $logoHeight;

        $logoX = imagesx($sourceImage) / 2 - $logoWidth / 2;
        $logoY = imagesy($sourceImage) / 2 - $logoHeight / 2;

        imagecopyresampled($sourceImage, $logoImage, (int) $logoX, (int) $logoY, 0, 0, $logoWidth, $logoHeight, $logoSourceWidth, $logoSourceHeight);

        return $sourceImage;
    }

    /**
     * @param resource $sourceImage
     * @param string $label
     * @param string|null $labelFontPath
     * @param int $labelFontSize
     * @param string $labelAlignment
     * @param array<string, int> $labelMargin
     * @param ValueInterface $foregroundColor
     * @param ValueInterface $backgroundColor
     * @return resource
     * @throws GenerateImageException
     * @throws MissingFunctionException
     */
    private function addLabel(
        $sourceImage,
        string $label,
        ?string $labelFontPath,
        int $labelFontSize,
        string $labelAlignment,
        array $labelMargin,
        ValueInterface $foregroundColor,
        ValueInterface $backgroundColor
    ) {
        if (!function_exists('imagettfbbox')) {
            throw new MissingFunctionException('Missing function "imagettfbbox", please make sure you installed the FreeType library');
        }
        
        if (null === $labelFontPath) {
            return $sourceImage;
        }

        $labelAlignment = LabelAlignment::from($labelAlignment);

        $labelBox = imagettfbbox($labelFontSize, 0, $labelFontPath, $label);
        $labelBoxWidth = (int) $labelBox[2] - $labelBox[0];
        $labelBoxHeight = (int) $labelBox[0] - $labelBox[7];

        $sourceWidth = imagesx($sourceImage);
        $sourceHeight = imagesy($sourceImage);
        $targetWidth = $sourceWidth;
        $targetHeight = $sourceHeight + $labelBoxHeight + $labelMargin['t'] + $labelMargin['b'];

        // Create empty target image
        $targetImage = imagecreatetruecolor($targetWidth, $targetHeight);

        if (!is_resource($targetImage)) {
            throw new GenerateImageException('Unable to generate image: check your GD installation');
        }

        $foregroundColor = imagecolorallocate(
            $targetImage,
            $foregroundColor->getRGB()->getValue('R'),
            $foregroundColor->getRGB()->getValue('G'),
            $foregroundColor->getRGB()->getValue('B')
        );
        $backgroundColor = imagecolorallocate(
            $targetImage,
            $backgroundColor->getRGB()->getValue('R'),
            $backgroundColor->getRGB()->getValue('G'),
            $backgroundColor->getRGB()->getValue('B')
        );
        imagefill($targetImage, 0, 0, $backgroundColor);

        // Copy source image to target image
        imagecopyresampled($targetImage, $sourceImage, 0, 0, 0, 0, $sourceWidth, $sourceHeight, $sourceWidth, $sourceHeight);

        $labelX = match ($labelAlignment) {
            LabelAlignment::LEFT => $labelMargin['l'],
            LabelAlignment::RIGHT => $targetWidth - $labelBoxWidth - $labelMargin['r'],
            default => (int) $targetWidth / 2 - $labelBoxWidth / 2,
        };

        $labelY = $targetHeight - $labelMargin['b'];
        imagettftext($targetImage, $labelFontSize, 0, $labelX, $labelY, $foregroundColor, $labelFontPath, $label);

        return $targetImage;
    }

    private function imageToString($image): string
    {
        ob_start();
        imagepng($image);

        return (string) ob_get_clean();
    }

    public static function getContentType(): string
    {
        return 'image/png';
    }

    public static function getSupportedExtensions(): array
    {
        return ['png'];
    }

    public function getName(): string
    {
        return 'png';
    }
}
