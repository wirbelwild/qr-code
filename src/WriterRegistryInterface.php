<?php

namespace BitAndBlack\QrCode;

use BitAndBlack\QrCode\Writer\WriterInterface;

interface WriterRegistryInterface
{
    public function addWriters(iterable $writers): void;

    public function addWriter(WriterInterface $writer): void;

    public function getWriter(string $name): WriterInterface;

    public function getWriters(): array;
}
