<?php

namespace BitAndBlack\QrCode;

use BaconQrCode\Common\ErrorCorrectionLevel as BaconErrorCorrectionLevel;
use DASPRiD\Enum\Exception\IllegalArgumentException;

enum ErrorCorrectionLevel: string
{
    case LOW = 'low';
    case MEDIUM = 'medium';
    case QUARTILE = 'quartile';
    case HIGH = 'high';

    /**
     * @return BaconErrorCorrectionLevel
     * @throws IllegalArgumentException
     */
    public function toBaconErrorCorrectionLevel(): BaconErrorCorrectionLevel
    {
        $name = strtoupper($this->value[0]);
        return BaconErrorCorrectionLevel::valueOf($name);
    }
}
