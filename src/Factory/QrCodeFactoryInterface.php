<?php

namespace BitAndBlack\QrCode\Factory;

use BitAndBlack\QrCode\QrCodeInterface;

interface QrCodeFactoryInterface
{
    public function create(string $text, array $options = []): QrCodeInterface;
}
