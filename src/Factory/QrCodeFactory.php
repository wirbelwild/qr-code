<?php

namespace BitAndBlack\QrCode\Factory;

use BitAndBlack\QrCode\ErrorCorrectionLevel;
use BitAndBlack\QrCode\QrCode;
use BitAndBlack\QrCode\QrCodeInterface;
use BitAndBlack\QrCode\WriterRegistryInterface;
use Color\Value\Exception\InvalidInputNumberException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;

class QrCodeFactory implements QrCodeFactoryInterface
{
    private ?OptionsResolver $optionsResolver = null;

    /**
     * @var array<int, string>
     */
    private array $definedOptions = [
        'writer',
        'writer_options',
        'size',
        'margin',
        'foreground_color',
        'background_color',
        'encoding',
        'round_block_size',
        'error_correction_level',
        'logo_path',
        'logo_width',
        'logo_height',
        'label',
        'label_font_size',
        'label_font_path',
        'label_alignment',
        'label_margin',
        'validate_result',
    ];

    /**
     * @param array<int, string> $defaultOptions
     * @param WriterRegistryInterface|null $writerRegistry
     */
    public function __construct(
        private readonly array $defaultOptions = [],
        private readonly ?WriterRegistryInterface $writerRegistry = null,
    ) {
    }

    /**
     * @param string $text
     * @param array<int, string> $options
     * @return QrCodeInterface
     * @throws InvalidInputNumberException
     */
    public function create(string $text = '', array $options = []): QrCodeInterface
    {
        $options = $this->getOptionsResolver()->resolve($options);
        $accessor = PropertyAccess::createPropertyAccessor();

        $qrCode = new QrCode($text);

        if ($this->writerRegistry instanceof WriterRegistryInterface) {
            $qrCode->setWriterRegistry($this->writerRegistry);
        }

        foreach ($this->definedOptions as $option) {
            if (isset($options[$option])) {
                if ('writer' === $option) {
                    $options['writer_by_name'] = $options[$option];
                    $option = 'writer_by_name';
                }

                if ('error_correction_level' === $option) {
                    $options[$option] = ErrorCorrectionLevel::from($options[$option]);
                }

                $accessor->setValue($qrCode, $option, $options[$option]);
            }
        }

        return $qrCode;
    }

    private function getOptionsResolver(): OptionsResolver
    {
        if (!$this->optionsResolver instanceof OptionsResolver) {
            $this->optionsResolver = $this->createOptionsResolver();
        }

        return $this->optionsResolver;
    }

    private function createOptionsResolver(): OptionsResolver
    {
        $optionsResolver = new OptionsResolver();
        $optionsResolver
            ->setDefaults($this->defaultOptions)
            ->setDefined($this->definedOptions)
        ;

        return $optionsResolver;
    }
}
