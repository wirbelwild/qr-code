<?php

namespace BitAndBlack\QrCode;

enum LabelAlignment: string
{
    case LEFT = 'left';
    case CENTER = 'center';
    case RIGHT = 'right';
}
