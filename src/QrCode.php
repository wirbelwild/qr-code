<?php

namespace BitAndBlack\QrCode;

use BaconQrCode\Encoder\Encoder;
use BitAndBlack\QrCode\Exception\InvalidPathException;
use BitAndBlack\QrCode\Exception\UnsupportedExtensionException;
use BitAndBlack\QrCode\Writer\WriterInterface;
use Color\Value\Exception\InvalidInputNumberException;
use Color\Value\RGB;
use Color\Value\ValueInterface;
use DASPRiD\Enum\Exception\IllegalArgumentException;

/**
 * @see \BitAndBlack\QrCode\Tests\QrCodeTest
 */
class QrCode implements QrCodeInterface
{
    private int $size = 300;
    private int $margin = 10;

    private ValueInterface $foregroundColor;

    private ValueInterface $backgroundColor;

    private string $encoding = 'UTF-8';
    private bool $roundBlockSize = true;
    private ErrorCorrectionLevel $errorCorrectionLevel;

    private ?string $logoPath = null;
    private ?int $logoWidth = null;
    private ?int $logoHeight = null;

    private ?string $label = null;
    private int $labelFontSize = 16;
    private ?string $labelFontPath = null;
    private LabelAlignment $labelAlignment;

    /**
     * @var array<string, int>
     */
    private array $labelMargin = [
        't' => 0,
        'r' => 10,
        'b' => 10,
        'l' => 10,
    ];

    private WriterRegistryInterface $writerRegistry;
    private ?WriterInterface $writer = null;
    private array $writerOptions = [];
    private bool $validateResult = false;

    /**
     * @throws InvalidInputNumberException
     */
    public function __construct(
        private string $text = '',
    ) {
        $this->errorCorrectionLevel = ErrorCorrectionLevel::LOW;
        $this->labelAlignment = LabelAlignment::CENTER;

        $this->createWriterRegistry();

        $this->backgroundColor = new RGB(255, 255, 255);
        $this->foregroundColor = new RGB(0, 0, 0);
    }

    public function setText(string $text): self
    {
        $this->text = $text;
        return $this;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setSize(int $size): self
    {
        $this->size = $size;
        return $this;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function setMargin(int $margin): self
    {
        $this->margin = $margin;
        return $this;
    }

    public function getMargin(): int
    {
        return $this->margin;
    }

    /**
     * @param ValueInterface $foregroundColor
     * @return QrCode
     */
    public function setForegroundColor(ValueInterface $foregroundColor): self
    {
        $this->foregroundColor = $foregroundColor;
        return $this;
    }

    /**
     * @return ValueInterface
     */
    public function getForegroundColor(): ValueInterface
    {
        return $this->foregroundColor;
    }

    /**
     * @param ValueInterface $backgroundColor
     * @return QrCode
     */
    public function setBackgroundColor(ValueInterface $backgroundColor): self
    {
        $this->backgroundColor = $backgroundColor;
        return $this;
    }

    /**
     * @return ValueInterface
     */
    public function getBackgroundColor(): ValueInterface
    {
        return $this->backgroundColor;
    }

    public function setEncoding(string $encoding): self
    {
        $this->encoding = $encoding;
        return $this;
    }

    public function getEncoding(): string
    {
        return $this->encoding;
    }

    public function setRoundBlockSize(bool $roundBlockSize): self
    {
        $this->roundBlockSize = $roundBlockSize;
        return $this;
    }

    public function getRoundBlockSize(): bool
    {
        return $this->roundBlockSize;
    }

    public function setErrorCorrectionLevel(ErrorCorrectionLevel $errorCorrectionLevel): self
    {
        $this->errorCorrectionLevel = $errorCorrectionLevel;
        return $this;
    }

    public function getErrorCorrectionLevel(): ErrorCorrectionLevel
    {
        return $this->errorCorrectionLevel;
    }

    /**
     * @param string $logoPath
     * @return QrCode
     * @throws InvalidPathException
     */
    public function setLogoPath(string $logoPath): self
    {
        $logoPath = realpath($logoPath);

        if (false === $logoPath || !is_file($logoPath)) {
            throw new InvalidPathException('Invalid logo path: ' . $logoPath);
        }

        $this->logoPath = $logoPath;
        return $this;
    }

    public function getLogoPath(): ?string
    {
        return $this->logoPath;
    }

    public function setLogoSize(int $logoWidth, int|null $logoHeight = null): self
    {
        $this->logoWidth = $logoWidth;
        $this->logoHeight = $logoHeight;
        return $this;
    }

    public function setLogoWidth(int $logoWidth): self
    {
        $this->logoWidth = $logoWidth;
        return $this;
    }

    public function getLogoWidth(): ?int
    {
        return $this->logoWidth;
    }

    public function setLogoHeight(int $logoHeight): self
    {
        $this->logoHeight = $logoHeight;
        return $this;
    }

    public function getLogoHeight(): ?int
    {
        return $this->logoHeight;
    }

    /**
     * @param string $label
     * @param int|null $labelFontSize
     * @param string|null $labelFontPath
     * @param string|null $labelAlignment
     * @param array|null $labelMargin
     * @return QrCode
     * @throws InvalidPathException
     */
    public function setLabel(
        string $label,
        int|null $labelFontSize = null,
        string|null $labelFontPath = null,
        string|null $labelAlignment = null,
        array|null $labelMargin = null
    ): self {
        $this->label = $label;

        if (null !== $labelFontSize) {
            $this->setLabelFontSize($labelFontSize);
        }

        if (null !== $labelFontPath) {
            $this->setLabelFontPath($labelFontPath);
        }

        if (null !== $labelAlignment) {
            $this->setLabelAlignment($labelAlignment);
        }

        if (null !== $labelMargin) {
            $this->setLabelMargin($labelMargin);
        }
        
        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabelFontSize(int $labelFontSize): self
    {
        $this->labelFontSize = $labelFontSize;
        return $this;
    }

    public function getLabelFontSize(): int
    {
        return $this->labelFontSize;
    }

    /**
     * @param string $labelFontPath
     * @return QrCode
     * @throws InvalidPathException
     */
    public function setLabelFontPath(string $labelFontPath): self
    {
        $resolvedLabelFontPath = (string) realpath($labelFontPath);

        if (!is_file($resolvedLabelFontPath)) {
            throw new InvalidPathException('Invalid label font path: ' . $labelFontPath);
        }

        $this->labelFontPath = $resolvedLabelFontPath;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabelFontPath(): ?string
    {
        return $this->labelFontPath;
    }

    public function setLabelAlignment(string $labelAlignment): self
    {
        $this->labelAlignment = LabelAlignment::from($labelAlignment);
        return $this;
    }

    public function getLabelAlignment(): string
    {
        return $this->labelAlignment->value;
    }

    /**
     * @param array<string, int> $labelMargin
     * @return $this
     */
    public function setLabelMargin(array $labelMargin): self
    {
        $this->labelMargin = array_merge($this->labelMargin, $labelMargin);
        return $this;
    }

    /**
     * @return array<string, int>
     */
    public function getLabelMargin(): array
    {
        return $this->labelMargin;
    }

    public function setWriterRegistry(WriterRegistryInterface $writerRegistry): QrCodeInterface
    {
        $this->writerRegistry = $writerRegistry;
        return $this;
    }

    public function setWriter(WriterInterface $writer): self
    {
        $this->writer = $writer;
        return $this;
    }

    /**
     * @param string|null $name
     * @return WriterInterface
     */
    public function getWriter(string|null $name = null): WriterInterface
    {
        if (null !== $name) {
            return $this->writerRegistry->getWriter($name);
        }

        if ($this->writer instanceof WriterInterface) {
            return $this->writer;
        }

        return $this->writerRegistry->getDefaultWriter();
    }

    public function setWriterOptions(array $writerOptions): self
    {
        $this->writerOptions = $writerOptions;
        return $this;
    }

    public function getWriterOptions(): array
    {
        return $this->writerOptions;
    }

    private function createWriterRegistry(): void
    {
        $this->writerRegistry = new WriterRegistry();
        $this->writerRegistry->loadDefaultWriters();
    }

    public function setWriterByName(string $name): static
    {
        $this->writer = $this->getWriter($name);
        return $this;
    }

    /**
     * @param string $path
     * @return QrCode
     * @throws UnsupportedExtensionException
     */
    public function setWriterByPath(string $path): self
    {
        $extension = pathinfo($path, PATHINFO_EXTENSION);

        $this->setWriterByExtension($extension);
        return $this;
    }

    /**
     * @param string $extension
     * @return QrCode
     * @throws UnsupportedExtensionException
     */
    public function setWriterByExtension(string $extension): self
    {
        foreach ($this->writerRegistry->getWriters() as $writer) {
            if ($writer->supportsExtension($extension)) {
                $this->writer = $writer;
                return $this;
            }
        }

        throw new UnsupportedExtensionException('Missing writer for extension "' . $extension . '"');
    }

    public function writeString(): string
    {
        return $this->getWriter()->writeString($this);
    }

    public function writeDataUri(): string
    {
        return $this->getWriter()->writeDataUri($this);
    }

    public function writeFile(string $path): QrCodeInterface
    {
        $this->getWriter()->writeFile($this, $path);
        return $this;
    }

    public function getContentType(): string
    {
        return $this->getWriter()->getContentType();
    }

    public function setValidateResult(bool $validateResult): self
    {
        $this->validateResult = $validateResult;
        return $this;
    }

    public function getValidateResult(): bool
    {
        return $this->validateResult;
    }

    /**
     * @return array
     * @throws IllegalArgumentException
     */
    public function getData(): array
    {
        $baconErrorCorrectionLevel = $this->errorCorrectionLevel->toBaconErrorCorrectionLevel();

        $baconQrCode = Encoder::encode($this->text, $baconErrorCorrectionLevel, $this->encoding);

        $matrix = $baconQrCode->getMatrix()->getArray()->toArray();

        foreach ($matrix as &$row) {
            $row = $row->toArray();
        }

        unset($row);

        $data = [
            'matrix' => $matrix,
        ];
        $data['block_count'] = null === $matrix[0] ? 0 : count($matrix[0]);
        $data['block_size'] = $this->size / $data['block_count'];
        
        if ($this->roundBlockSize) {
            $data['block_size'] = (int) floor($data['block_size']);
        }
        
        $data['inner_width'] = $data['block_size'] * $data['block_count'];
        $data['inner_height'] = $data['block_size'] * $data['block_count'];
        $data['outer_width'] = $this->size + 2 * $this->margin;
        $data['outer_height'] = $this->size + 2 * $this->margin;
        $data['margin_left'] = ($data['outer_width'] - $data['inner_width']) / 2;
        
        if ($this->roundBlockSize) {
            $data['margin_left'] = (int) floor($data['margin_left']);
        }
        
        $data['margin_right'] = $data['outer_width'] - $data['inner_width'] - $data['margin_left'];

        return $data;
    }
}
