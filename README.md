[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/qr-code)](http://www.php.net)
[![Latest Stable Version](https://poser.pugx.org/bitandblack/qr-code/v/stable)](https://packagist.org/packages/bitandblack/qr-code)
[![Total Downloads](https://poser.pugx.org/bitandblack/qr-code/downloads)](https://packagist.org/packages/bitandblack/qr-code)
[![License](https://poser.pugx.org/bitandblack/qr-code/license)](https://packagist.org/packages/bitandblack/qr-code)

# Bit&Black QR Code

This library generates QR codes. It's based on [endroid/qr-code](https://packagist.org/packages/endroid/qr-code) by Jeroen van den Enden but differs by using [bitandblack/colors](https://packagist.org/packages/bitandblack/colors) to allow different color spaces.

## Installation

This library is made for the use with [Composer](https://packagist.org/packages/bitandblack/qr-code). Add it to your project by running `$ composer require bitandblack/qr-code`.

## Usage

```php
<?php 

use BitAndBlack\QrCode\QrCode;

$qrCode = new QrCode('Hello World!');

header('Content-Type: ' . $qrCode->getContentType());

echo $qrCode->writeString();
```

### Advanced usage

```php
<?php 

use Color\Value\RGB;
use BitAndBlack\QrCode\ErrorCorrectionLevel;
use BitAndBlack\QrCode\LabelAlignment;
use BitAndBlack\QrCode\QrCode;
use BitAndBlack\QrCode\Response\QrCodeResponse;

$qrCode = new QrCode('Hello World!');
$qrCode
    ->setSize(300)    
    ->setWriterByName('png')    
    ->setMargin(10)    
    ->setEncoding('UTF-8')    
    ->setErrorCorrectionLevel(
        new ErrorCorrectionLevel(ErrorCorrectionLevel::HIGH())
    )    
    ->setForegroundColor(
        new RGB(0, 0, 0)
    )    
    ->setBackgroundColor(
        new RGB(255, 255, 255)
    )    
    ->setLabel('Scan the code', 16, 'SomeFont.otf', LabelAlignment::CENTER)    
    ->setLogoPath('SomeLogo.png')    
    ->setLogoSize(150, 200)    
    ->setRoundBlockSize(true)    
    ->setValidateResult(false)    
    ->setWriterOptions([
        'exclude_xml_declaration' => true
    ])
;

// Directly output the QR code
header('Content-Type: ' . $qrCode->getContentType());

echo $qrCode->writeString();

// Save it to a file
$qrCode->writeFile(__DIR__ . '/qrcode.png');

// Create a response object
$response = new QrCodeResponse($qrCode);
```

## Help 

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).
