<?php

namespace BitAndBlack\QrCode\Tests;

use BitAndBlack\QrCode\Exception\InvalidPathException;
use BitAndBlack\QrCode\Exception\UnsupportedExtensionException;
use BitAndBlack\QrCode\Factory\QrCodeFactory;
use BitAndBlack\QrCode\QrCode;
use DASPRiD\Enum\Exception\IllegalArgumentException;
use Iterator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Zxing\QrReader;

class QrCodeTest extends TestCase
{
    public function testReadable(): void
    {
        $messages = [
            'Tiny',
            'This one has spaces',
            'd2llMS9uU01BVmlvalM2YU9BUFBPTTdQMmJabHpqdndt',
            'http://this.is.an/url?with=query&string=attached',
            '11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111',
            '{"i":"serialized.data","v":1,"t":1,"d":"4AEPc9XuIQ0OjsZoSRWp9DRWlN6UyDvuMlyOYy8XjOw="}',
            'Spëci&al ch@ract3rs',
            '有限公司',
        ];

        $qrCode = new QrCode();
        $qrCode->setSize(300);

        foreach ($messages as $message) {
            $qrCode->setText($message);
            $pngData = $qrCode->writeString();

            self::assertIsString($pngData);

            $reader = new QrReader(
                $pngData,
                QrReader::SOURCE_TYPE_BLOB
            );

            self::assertEquals(
                $message,
                $reader->text()
            );
        }
    }

    public function testFactory(): void
    {
        $qrCodeFactory = new QrCodeFactory();
        $qrCode = $qrCodeFactory->create('QR Code', [
            'writer' => 'png',
            'size' => 300,
            'margin' => 10,
        ]);

        $pngData = $qrCode->writeString();
        self::assertIsString($pngData);
        $reader = new QrReader($pngData, QrReader::SOURCE_TYPE_BLOB);
        self::assertSame('QR Code', $reader->text());
    }

    /**
     * @param string $writerName
     * @param string|null $fileContent
     * @throws InvalidPathException
     */
    #[DataProvider('writerNamesProvider')]
    public function testWriteQrCodeByWriterName(string $writerName, ?string $fileContent): void
    {
        $qrCode = new QrCode('QR Code');
        $qrCode->setLogoPath(__DIR__ . '/symfony.png');
        $qrCode->setLogoWidth(100);

        $qrCode->setWriterByName($writerName);
        $data = $qrCode->writeString();
        self::assertIsString($data);

        if (null !== $fileContent) {
            $uriData = $qrCode->writeDataUri();
            self::assertSame(0, strpos($uriData, (string) $fileContent));
        }
    }

    public static function writerNamesProvider(): Iterator
    {
        yield ['binary', null];
        yield ['debug', null];
        yield ['eps', null];
        yield ['png', 'data:image/png;base64'];
        yield ['svg', 'data:image/svg+xml;base64'];
    }

    /**
     * @param string $extension
     * @param string|null $fileContent
     * @throws InvalidPathException
     * @throws UnsupportedExtensionException
     */
    #[DataProvider('extensionsProvider')]
    public function testWriteQrCodeByWriterExtension(string $extension, ?string $fileContent): void
    {
        $qrCode = new QrCode('QR Code');
        $qrCode->setLogoPath(__DIR__ . '/symfony.png');
        $qrCode->setLogoWidth(100);

        $qrCode->setWriterByExtension($extension);
        $data = $qrCode->writeString();
        self::assertIsString($data);

        if (null !== $fileContent) {
            $uriData = $qrCode->writeDataUri();
            self::assertSame(0, strpos($uriData, (string) $fileContent));
        }
    }

    public static function extensionsProvider(): Iterator
    {
        yield ['bin', null];
        yield ['txt', null];
        yield ['eps', null];
        yield ['png', 'data:image/png;base64'];
        yield ['svg', 'data:image/svg+xml;base64'];
    }

    public function testSetSize(): void
    {
        $size = 400;
        $margin = 10;

        $qrCode = new QrCode('QR Code');
        $qrCode->setSize($size);
        $qrCode->setMargin($margin);

        $pngData = $qrCode->writeString();
        $image = imagecreatefromstring($pngData);

        self::assertSame(imagesx($image), $size + 2 * $margin);
        self::assertSame(imagesy($image), $size + 2 * $margin);
    }

    /**
     * @throws InvalidPathException
     */
    public function testSetLabel(): void
    {
        $qrCode = new QrCode('QR Code');
        $qrCode->setSize(300);
        $qrCode->setLabel('Scan the code', 15);

        $pngData = $qrCode->writeString();
        self::assertIsString($pngData);
        $reader = new QrReader($pngData, QrReader::SOURCE_TYPE_BLOB);
        self::assertSame('QR Code', $reader->text());
    }

    /**
     * @throws InvalidPathException
     */
    public function testSetLogo(): void
    {
        $qrCode = new QrCode('QR Code');
        $qrCode->setSize(500);
        $qrCode->setLogoPath(__DIR__ . '/symfony.png');
        $qrCode->setLogoWidth(100);
        $qrCode->setValidateResult(true);

        $pngData = $qrCode->writeString();
        self::assertIsString($pngData);
    }

    public function testWriteFile(): void
    {
        $filename = __DIR__ . '/output/qr-code.png';

        $qrCode = new QrCode('QR Code');
        $qrCode->writeFile($filename);

        $image = imagecreatefromstring(file_get_contents($filename));

        self::assertNotFalse($image);
    }

    /**
     * @throws IllegalArgumentException
     */
    public function testData(): void
    {
        $qrCode = new QrCode('QR Code');

        $data = $qrCode->getData();

        self::assertArrayHasKey('block_count', $data);
        self::assertArrayHasKey('block_size', $data);
        self::assertArrayHasKey('inner_width', $data);
        self::assertArrayHasKey('inner_height', $data);
        self::assertArrayHasKey('outer_width', $data);
        self::assertArrayHasKey('outer_height', $data);
        self::assertArrayHasKey('margin_left', $data);
        self::assertArrayHasKey('margin_right', $data);
    }
}
