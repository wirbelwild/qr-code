<?php

namespace BitAndBlack\QrCode\Tests\Writer;

use BitAndBlack\QrCode\QrCode;
use BitAndBlack\QrCode\Writer\SvgWriter;
use Color\Value\RGB;
use PHPUnit\Framework\TestCase;

/**
 * Class SvgWriterTest
 *
 * @package BitAndBlack\QrCode\Tests\Writer
 */
class SvgWriterTest extends TestCase
{
    /**
     * Tests if HEX colors can be added successfully
     */
    public function testCanHandleHEXColors()
    {
        $backgroundColor = [0, 4, 4];
        $foregroundColor = [0, 255, 200];

        $backgroundColorRGB = new RGB(...$backgroundColor);
        $foregroundColorRGB = new RGB(...$foregroundColor);
        
        $qrCode = new QrCode();
        $qrCode->setWriter(new SvgWriter());
        $qrCode
            ->setBackgroundColor($backgroundColorRGB)
            ->setForegroundColor($foregroundColorRGB)
        ;

        $string = $qrCode->writeString();

        self::assertStringContainsString(
            'fill="' . $foregroundColorRGB->getHEX() . '"',
            $string,
            'No hex color found'
        );

        self::assertStringContainsString(
            'fill="' . $backgroundColorRGB->getHEX() . '"',
            $string,
            'No hex color found'
        );
    }
}
