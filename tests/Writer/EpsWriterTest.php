<?php

namespace BitAndBlack\QrCode\Tests\Writer;

use BitAndBlack\QrCode\QrCode;
use BitAndBlack\QrCode\Writer\EpsWriter;
use Color\Value\CMYK;
use Color\Value\RGB;
use PHPUnit\Framework\TestCase;

/**
 * Class EpsWriterTest
 *
 * @package BitAndBlack\QrCode\Tests\Writer
 */
class EpsWriterTest extends TestCase
{
    /**
     * Tests if RGB colors can be added successfully
     */
    public function testCanHandleRGBColors()
    {
        $backgroundColor = [0, 4, 4];
        $foregroundColor = [0, 255, 200];

        $backgroundColorRGB = new RGB(...$backgroundColor);
        $foregroundColorRGB = new RGB(...$foregroundColor);

        $qrCode = new QrCode();
        $qrCode->setWriter(new EpsWriter());
        $qrCode
            ->setBackgroundColor($backgroundColorRGB)
            ->setForegroundColor($foregroundColorRGB)
        ;

        $string = $qrCode->writeString();
        $colorLines = [];

        foreach (preg_split("/((\r?\n)|(\r\n?))/", $string) as $line) {
            if (str_contains($line, 'setrgbcolor')) {
                $colorLines[] = $line;
            }
        }

        $backgroundColor = array_map(
            static fn ($colorValue) => $colorValue / 255,
            $backgroundColor
        );
        
        $foregroundColor = array_map(
            static fn ($colorValue) => $colorValue / 255,
            $foregroundColor
        );
        
        self::assertEquals(
            $colorLines[0] ?? '',
            implode(' ', $backgroundColor) . ' setrgbcolor'
        );

        self::assertEquals(
            $colorLines[1] ?? '',
            implode(' ', $foregroundColor) . ' setrgbcolor'
        );
    }
    
    /**
     * Tests if CMYK colors can be added successfully
     */
    public function testCanHandleCMYKColors()
    {
        $backgroundColor = [0, 5, 5, 0];
        $foregroundColor = [0, 100, 100, 0];

        $backgroundColorCMYK = new CMYK(...$backgroundColor);
        $foregroundColorCMYK = new CMYK(...$foregroundColor);

        $qrCode = new QrCode();
        $qrCode
            ->setWriter(new EpsWriter())
            ->setBackgroundColor($backgroundColorCMYK)
            ->setForegroundColor($foregroundColorCMYK)
        ;

        $string = $qrCode->writeString();
        $colorLines = [];

        foreach (preg_split("/((\r?\n)|(\r\n?))/", $string) as $line) {
            if (str_contains($line, 'setcmykcolor')) {
                $colorLines[] = $line;
            }
        }

        $backgroundColor = array_map(
            static fn ($colorValue) => $colorValue / 100,
            $backgroundColor
        );

        $foregroundColor = array_map(
            static fn ($colorValue) => $colorValue / 100,
            $foregroundColor
        );

        self::assertSame(
            $colorLines[0],
            implode(' ', $backgroundColor) . ' setcmykcolor'
        );

        self::assertSame(
            $colorLines[1],
            implode(' ', $foregroundColor) . ' setcmykcolor'
        );
    }
}
